package com.telefonica.testelasticsearch.entity;

import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;

import lombok.Data;

@Data
@Document(indexName = "customer", type = "default")
public class Customer {
    @Id
    private Integer id;
    private String name;
    private String dni;
}
