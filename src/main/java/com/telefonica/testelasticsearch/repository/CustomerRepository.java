package com.telefonica.testelasticsearch.repository;

import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

import com.telefonica.testelasticsearch.entity.Customer;

@Repository
public interface CustomerRepository extends ElasticsearchRepository<Customer, Integer> {

}
