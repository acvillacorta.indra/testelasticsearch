package com.telefonica.testelasticsearch.controller;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.telefonica.testelasticsearch.entity.Customer;
import com.telefonica.testelasticsearch.repository.CustomerRepository;

@RestController
public class CustomerController {

    @Autowired
    private CustomerRepository repository;

    @PostMapping("/customer/add")
    public Customer addCustomer(@RequestBody Customer customer) {
	return repository.save(customer);
    }

    @GetMapping("/customer/all")
    public List<Customer> getCustomers() {
	Iterator<Customer> iterator = repository.findAll().iterator();
	List<Customer> customers = new ArrayList<>();
	while (iterator.hasNext()) {
	    customers.add(iterator.next());
	}
	return customers;
    }

    @GetMapping("/customer/{id}")
    public Optional<Customer> getCustomer(@PathVariable Integer id) {
	return repository.findById(id);
    }

    @PutMapping("/customer/{id}")
    public Customer updateCustomer(@PathVariable Integer id, @RequestBody Customer customer) {
	Optional<Customer> cus = repository.findById(id);
	if (cus.isPresent()) {
	    Customer c = cus.get();
	    c.setName(customer.getName());
	    return repository.save(c);
	} else
	    return null;
    }

    @DeleteMapping("/customer/{id}")
    public String deleteCustomer(@PathVariable Integer id) {
	repository.deleteById(id);
	return "Document Deleted";
    }
}
