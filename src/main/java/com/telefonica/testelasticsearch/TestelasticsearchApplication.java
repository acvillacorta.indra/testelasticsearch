package com.telefonica.testelasticsearch;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestelasticsearchApplication {

	public static void main(String[] args) {
		SpringApplication.run(TestelasticsearchApplication.class, args);
	}

}
